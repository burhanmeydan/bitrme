
package com.teklifver.Controller;

/**
 * Created by hasan on 17.04.2018.
 */

import com.teklifver.Services.*;
import com.teklifver.data.CodeCheckService;
import com.teklifver.data.UserData;
import com.teklifver.entity.*;
import com.teklifver.form.*;
import com.teklifver.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.mail.*;
import javax.mail.internet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.*;

@Controller
public class LoginPageController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private TownService townService;

    @Autowired
    private DistrictService districtService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private SubCategoryService subCategoryService;

    @Autowired
    private UserService userService;

    @Autowired
    private PostService postService;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private TownRepository townRepository;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private CodeCheckService codeCheckService;


    @GetMapping("/chooseLogin")
    public String chooseLogin(){
        return "pages/chooseLoginPage";
    }

    @GetMapping(value = "advisermentInformation/{id}")
    public String advisermentInformation(@PathVariable Long id, Model model){

        PostEntity postEntity = postService.getPostEntity(id);
        model.addAttribute("posts",postEntity);
        return "pages/advisertmentInformation";
    }

    @RequestMapping(value = "myAccount/{id}",method = RequestMethod.GET)
    public String myAccount(@PathVariable Long id, Model model){

        UserEntity userEntity = userRepository.findOne(id);
        Iterable<PostEntity> postEntities = postRepository.findAll();
        List<PostEntity> postEntityList = new ArrayList<>();
        for (PostEntity postEntity : postEntities){
            if(postEntity.getUserId().equals(String.valueOf(id))){
                postEntityList.add(postEntity);
            }
        }
        Iterable<CityEntity> cityEntities = cityRepository.findAll();
        Iterable<TownEntity> townEntities = townRepository.findAll();
        model.addAttribute("posts", postEntityList);
        model.addAttribute("towns",townEntities);
        model.addAttribute("cities",cityEntities);
        model.addAttribute("id",id);
        model.addAttribute("user",userEntity);
        model.addAttribute("postForm",new PostForm());
        model.addAttribute("userUyeForm", new UserUyeForm());
        return "pages/myAccount";
    }


    @RequestMapping(value = "userAdvisermentList/{id}",method = RequestMethod.GET)
    public String userAdvisermentList(@PathVariable Long id, Model model){

        model.addAttribute("id",id);
        return "pages/userAdvisermentList";
    }
    @GetMapping("/userAdvisermentList")
    public String userAdvisermentList(){
        return "pages/userAdvisermentList";
    }

    @RequestMapping(value = "notifications/{id}",method = RequestMethod.GET)
    public String notifications(@PathVariable Long id,Model model)
    {
        model.addAttribute("id" ,id);
        return "pages/notifications";
    }

    @RequestMapping(value = "editProfile/{id}", method = RequestMethod.POST)
    public String editProfile(@Valid UserUyeForm userUyeForm, @PathVariable Long id) {
        UserEntity userEntity = userRepository.findOne(id);
        UserEntity user = userRepository.findUserEntityByEmail(userUyeForm.getMail());
        if (user == null){
            UserEntity user2 = geteditProfile(userEntity,userUyeForm);
            user2.setEmail(userUyeForm.getMail());
            userRepository.save(user2);
            return "redirect:/myAccount/{id}";

        }
        else {
              UserEntity user2=  geteditProfile(userEntity,userUyeForm);
              userRepository.save(user2);
              return "redirect:/myAccount/{id}";
        }
    }

    @RequestMapping(value = "editAddress/{id}", method = RequestMethod.POST)
    public String editAddress(@Valid UserUyeForm userUyeForm,@PathVariable Long id){
        if (!userUyeForm.getCityName().isEmpty() && !userUyeForm.getTownName().isEmpty()
                && !userUyeForm.getLine().isEmpty()){
            UserEntity userEntity = userRepository.findOne(id);
            userEntity.setCityName(userUyeForm.getCityName());
            userEntity.setTownName(userUyeForm.getTownName());
            userEntity.setAddressLine(userUyeForm.getLine());
            userRepository.save(userEntity);
        }

        return "redirect:/myAccount/{id}";
    }


    public  UserEntity geteditProfile(UserEntity userEntity, UserUyeForm userUyeForm){
        if (!userEntity.getLastName().equals(userUyeForm.getMail())) {
            if (!userUyeForm.getLastname().isEmpty()) {
                userEntity.setLastName(userUyeForm.getLastname());
            }
            if (!userUyeForm.getName().isEmpty()) {
                userEntity.setName(userUyeForm.getName());
            }
        }
        return userEntity;
    }
        @GetMapping("/personalLogin")
    public String personalLogin(Model model){

        model.addAttribute("loginForm",new LoginForm());
        return "pages/personalLogin";
    }
    @GetMapping("/companyLogin")
    public String companyLogin(Model model){

//        List<CityEntity> list=new ArrayList<>();
//        Iterable<CityEntity>  cityEntities=cityRepository.findAll();
//        for (CityEntity p:cityEntities)
//        {
//            list.add(p);
//        }
//        model.addAttribute("popUp",true);
//        model.addAttribute("cities",list);
//        model.addAttribute("individualRegisterForm", new IndividualRegisterForm());
        return "pages/companyLogin";
    }
    @GetMapping("/personalRegister")
    public String personalRegister(Model model){

        Iterable<CityEntity>  cityEntities=cityRepository.findAll();
        Iterable<TownEntity> townEntities = townRepository.findAll();

        model.addAttribute("towns", townEntities);
        model.addAttribute("popUp",true);
        model.addAttribute("cities",cityEntities);
        model.addAttribute("individualRegisterForm", new IndividualRegisterForm());
        return "pages/personalRegister";
    }
    @GetMapping("/companyRegister")
    public String companyRegister(Model model)
    {
        List<CategoryEntity> categoryEntities = new ArrayList<>();
        Iterable<CategoryEntity> categoryEntityIterable = categoryRepository.findAll();
        for (CategoryEntity categoryEntity : categoryEntityIterable)
        {
            categoryEntities.add(categoryEntity);
        }
        List<CityEntity> list=new ArrayList<>();
        Iterable<CityEntity>  cityEntities=cityRepository.findAll();
        Iterable<TownEntity>  townEntities = townRepository.findAll();
        for (CityEntity p:cityEntities)
        {
            list.add(p);
        }
        model.addAttribute("categories",categoryEntities);
        model.addAttribute("cities",list);
        model.addAttribute("towns",townEntities);
        model.addAttribute("companyRegisterForm",new CompanyRegisterForm());
        return "pages/companyRegister";
    }
    @GetMapping("/chooseRegister")
    public String chooseRegister(){
        return "pages/chooseRegisterPage";
    }
    @GetMapping("/forgotPassword")
    public String forgotPassword( Model model){

        model.addAttribute("sendEmailForm",new SendEmailForm());
        return "pages/forgotPasswordPage";
    }


    @PostMapping("/individualLogin")
    private String Login(@ModelAttribute LoginForm loginForm,HttpServletRequest request)
    {

        UserEntity userEntity = userRepository.findAllByEmail(loginForm.getMail());

        if (userEntity != null && userEntity.getPassword().equals(loginForm.getPassword()))
        {
            UserData userData = userService.populateUserDat(userEntity);
            HttpSession session = request.getSession();
            session.setAttribute("customer",userData);

            return "redirect:/";
        }
        return "redirect:/personalLogin";
    }
    @PostMapping("/editPost")
    public String editPost(@ModelAttribute PostForm postForm, HttpServletRequest request){

        postService.updatePostEntity(postForm,request);
        return "redirect:/pages/userAdvisermentInformation";
    }



    @PostMapping("/giris")
    public String Giris(@ModelAttribute LoginForm loginForm,Model model,HttpServletRequest request)
    {


//        HttpSession session=request.getSession();
//        request.getSession().getAttribute("username");
//        Iterable<Users>  users=userRepository.findAllByMail(loginForm.getMail());
//        Users users1=users.iterator().next();
//        session.setAttribute("username",users1.getName()+" "+users1.getLastName());
//
//
//        if(users == null)
//        {
//            return "Login";
//        }
//        else if(!users1.getPassword().equals(loginForm.getPassword()))
//        {
//            return "Login";
//        }
//        else if(users1.getPassword().equals(loginForm.getPassword()))
//        {
//
//            session.setAttribute("kisi_id",users1.getId());
//            session.setAttribute("il",users1.getIl());
//            session.setAttribute("ilce",users1.getIlce());
//            session.setAttribute("mahalle",users1.getLastName());
//            session.setAttribute("loginorexit","cıkısyap");
//            session.setAttribute("loginorexit","cıkısyap");
//            String login=(String)request.getSession().getAttribute("loginorexit");
//            model.addAttribute("logins",login);
//            model.addAttribute("isim",session.getAttribute("username"));
//            return "index";
//
//        }
//        else
            return null;
    }

    @GetMapping("/town/{cityName}")
    @ResponseBody
    public List<TownEntity> getTown(@PathVariable String cityName)
    {
        CityEntity cityEntity = cityRepository.findByCityName(cityName);
        return townService.getTownByProvinceId(cityEntity.getId().toString());
    }

//    @GetMapping("/district/{townId}")
//    @ResponseBody
//    public List<DistrictEntity> getDistrict(@PathVariable int townId)
//    {
//        return districtService.getDistrictByTownId(townId);
//    }

    @GetMapping("/category/{categoryId}")
    @ResponseBody
    public List<SubCategoryEntity>  getSubCategories(@PathVariable int categoryId,Model model)
    {
        return subCategoryService.getSubCategoryByCategoryId(categoryId);
    }

    public  String getRandomNumberString() {
        // It will generate 6 digit random Number.
        // from 0 to 999999
        Random rnd = new Random();
        int number = rnd.nextInt(999999);

        // this will convert any number sequence into 6 character.
        return String.format("%06d", number);
    }

    @PostMapping("/sendEmail")
    public String sendEmail(@ModelAttribute("sendEmailForm") @Valid SendEmailForm sendEmailForm, BindingResult bindingResult, RedirectAttributes redirectAttributes,Model model,HttpServletRequest request) throws MailException, IOException, MessagingException {
        if (bindingResult.hasErrors())
        {
            redirectAttributes.addFlashAttribute("org.springframework.validation.SendEmailForm.", bindingResult);
            return "pages/forgotPasswordPage";
        }
        UserEntity userEntity = userRepository.findUserEntityByEmail(sendEmailForm.getEmail());
        if (userEntity == null){
            return "redirect:/forgotPassword";
        }
        HttpSession session = request.getSession();
        session.setAttribute("email",sendEmailForm.getEmail());
        sendmaill(sendEmailForm.getEmail());
        model.addAttribute("verificationCodeForm",new VerificationCodeForm());
        return "pages/checkVerificationCodePage";
    }
    private void sendmaill(String toEmail) throws AddressException, MessagingException, IOException {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("teklifverr@gmail.com", "teklifver123");
            }
        });
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress("hasan.altun@hibizz.com", false));
        String code = getRandomNumberString();
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
        msg.setSubject("Şifre Değiştirme");
        msg.setContent("Şifre değiştirme için gereken doğrulama kodu :" + code , "text/plain; charset=UTF-8");
        msg.setSentDate(new Date());

        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent("Şifre değiştirme için gereken doğrulama kodu :" + code, "text/plain; charset=UTF-8");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        msg.setContent(multipart);
        Transport.send(msg);
        codeCheckService.addOtpCode(toEmail,code);
    }

    @PostMapping("/checkVerificationCode")
    private String checkVerificationcode(@ModelAttribute("verificationCodeForm") @Valid VerificationCodeForm verificationCodeForm , BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model, HttpServletRequest request)
    {
        if (bindingResult.hasErrors())
        {
            redirectAttributes.addFlashAttribute("org.springframework.validation.SendEmailForm.", bindingResult);
            return "pages/checkVerificationCodePage";
        }

        HttpSession session = request.getSession();
        if (codeCheckService.getOtpCode((String) session.getAttribute("email")).equals(verificationCodeForm.getVerificationCode()))
        {
            model.addAttribute("checkPasswordForm",new CheckPasswordForm());
            return "pages/checkPasswordPage";
        }
        else {
            redirectAttributes.addFlashAttribute("isFail",Boolean.TRUE);
            return "redirect:/";
        }
//        return "";
    }

    @PostMapping("/editPassword")
    private String editPassword(@ModelAttribute("checkPasswordForm") @Valid CheckPasswordForm checkPasswordForm, HttpServletRequest request)
    {
        HttpSession session = request.getSession();
        String mail = (String) session.getAttribute("email");

        UserEntity userEntity = userRepository.findAllByEmail(mail);
        userEntity.setPassword(checkPasswordForm.getPassword());
        userRepository.save(userEntity);

        return "redirect:/";
    }

}
