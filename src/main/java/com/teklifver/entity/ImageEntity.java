package com.teklifver.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "images")
public class ImageEntity extends BaseEntity {

    private long post_id;

    private String fileName;

    public long getPost_id() {
        return post_id;
    }

    public void setPost_id(long post_id) {
        this.post_id = post_id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
