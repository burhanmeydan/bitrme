package com.teklifver.Services;

import com.teklifver.entity.CompanyEntity;
import com.teklifver.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    public void save(CompanyEntity companyEntity){

        companyRepository.save(companyEntity);
    }
}
