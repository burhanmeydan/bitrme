package com.teklifver.repository;

import com.teklifver.entity.CategoryEntity;
import com.teklifver.entity.SubCategoryEntity;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<CategoryEntity,Long> {
       CategoryEntity findCategoryEntitiesByCategoryName(String categoryName);
       CategoryEntity findCategoryEntityBy(String categoryName);
}
