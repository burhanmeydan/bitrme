package com.teklifver.repository;

import com.teklifver.entity.ImageEntity;
import org.springframework.data.repository.CrudRepository;

public interface ImageRepository extends CrudRepository<ImageEntity,Long>
{

}